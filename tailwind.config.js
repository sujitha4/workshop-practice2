module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      textColor:{
        textwhite: '#FFFFFF',
        textblack: ' #242635',
        textbuttn:'#111111',
        textBlk:'#000000',
        textBlue:'#8C9DC5',
        
      },
      colors: {
        regalblue: '#0B0F15',
        backorenge: '#FF6E43',
        backRed: '#FF6E43',
        backgray:'#5F5F5F',
        bgGray:'#D9DAE4',
        
      },

    },
  },
  plugins: [],
}
// module.exports = {
//   theme: {
//     extend: {
//       colors: {
//         'regal-blue': '#243c5a',
//       },
//     }
//   }
// }
